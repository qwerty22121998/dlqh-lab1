﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1
{
    class Tester : Employee
    {
        private int exp { get; set; }

        public Tester(int id, string name, double salary, int exp) : base(id, name, salary)
        {
            this.exp = exp;
        }
        public override double GetSalary()
        {
            return base.Salary * (12 + exp * 0.2);
        }
        public override string ToString()
        {
            return $"{base.ToString()}|{exp}";
        }

    }
}
