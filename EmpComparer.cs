﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1
{
    class EmpComparerName : IComparer<Employee>
    {
        public int Compare(Employee x, Employee y)
        {
            return String.Compare(x.Name, y.Name);
        }
    }
    class EmpCompareID : IComparer<Employee>
    {
        public int Compare(Employee x, Employee y)
        {
            return x.Id - y.Id;
        }
    }

    class EmpCompareSalary : IComparer<Employee>
    {
        public int Compare(Employee x, Employee y)
        {
            if (x.Salary == y.Salary) return 0;
            return x.Salary > y.Salary ? 1 : -1;
        }
    }

}
